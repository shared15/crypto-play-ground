﻿using System;
using Library;

namespace Driver
{
    class Program
    {
        public static void Main(string[] passwordargs)
        {
            var password = "test";
            var keyGenerator = new KeyGenerator();
            var privateKeyXml = keyGenerator.GetPrivateKey();
            var publicKeyXml = keyGenerator.GetPublicKey();

            Console.WriteLine($"Private key as xml string: {privateKeyXml}");

            var saltGenerator = new SecureSaltGenerator();
            var salt64 = saltGenerator.GenerateSecureSalt();
            var symmetricKeygenerator = new SymmetricKeyGenerator();
            var symmetricKey64 = symmetricKeygenerator.GenerateKey(
                password,
                salt64
            );

            var privateKeyEncryptor = new PrivateKeyEncryptor();
            var encryptedPrivateKey64 = privateKeyEncryptor.Encrypt(
                privateKeyXml,
                symmetricKey64
            );

            Console.WriteLine($"Encrypted private key in base64: {encryptedPrivateKey64}");

            var privateKeyDecryptor = new PrivateKeyDecryptor();
            var decryptedkey = privateKeyDecryptor.Decrypt(
                encryptedPrivateKey64,
                symmetricKey64
            );

            Console.WriteLine($"Decrypted private key: {decryptedkey}");

            var dummyData = "This is a test.";
            var dataEncryption = new DataEncryption();
            var encryptedData = dataEncryption.Encrypt(
                dummyData,
                publicKeyXml
            );

            Console.WriteLine($"The message '{dummyData}' was encrypted to: {encryptedData}");

            var dataDecryption = new DataDecryption();
            var decryptedData = dataDecryption.Decrypt(
                encryptedData,
                privateKeyXml
            );

            Console.WriteLine($"The encrypted data was decrypted to: {decryptedData}");
        }
    }
}
