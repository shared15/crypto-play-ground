using System;
using System.Security.Cryptography;

namespace Library
{
    public class KeyGenerator
    {
        private readonly RSACryptoServiceProvider _rsaCryptoProvider;
        private const int KEY_SIZE = 3072;

        public KeyGenerator()
        {
            _rsaCryptoProvider = new RSACryptoServiceProvider(KEY_SIZE);
        }

        public string GetPublicKey()
        {
            return _rsaCryptoProvider.ToXmlString(false);
        }


        public string GetPrivateKey()
        {
            return _rsaCryptoProvider.ToXmlString(true);
        }
    }
}