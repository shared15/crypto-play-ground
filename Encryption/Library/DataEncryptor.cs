using System;
using System.Security.Cryptography;
using System.Text;

namespace Library
{
    public class DataEncryption : IDisposable
    {
        private readonly RSACryptoServiceProvider _rsaCryptoProvider;
        private readonly UTF8Encoding _converter;

        public DataEncryption()
        {
            _rsaCryptoProvider = new RSACryptoServiceProvider();
            _converter = new UTF8Encoding();
        }

        public string Encrypt(string data, string publicKeyXml)
        {
            _rsaCryptoProvider.FromXmlString(publicKeyXml);
            var bytes = _converter.GetBytes(data);
            var encrypted = _rsaCryptoProvider.Encrypt(bytes, false);
            return Convert.ToBase64String(encrypted);
        }

        public void Dispose()
        {
            _rsaCryptoProvider.Dispose();
        }
    }
}