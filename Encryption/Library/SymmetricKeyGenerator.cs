using System;
using System.Security.Cryptography;

namespace Library
{
    public class SymmetricKeyGenerator
    {
        private const int ITERATIONS = 5000;
        public string GenerateKey(string password, string salt64)
        {
            var salt = Convert.FromBase64String(salt64);
            var pbkd = new Rfc2898DeriveBytes(password, salt, ITERATIONS);
            var key = pbkd.GetBytes(16);
            return Convert.ToBase64String(key);
        }
    }
}