using System.Security.Cryptography;
using System;
using System.IO;
using System.Text;

namespace Library
{
    public class PrivateKeyEncryptor
    {
        /// <summary>
        /// Encrypts the given private key using AES.
        /// </summary>
        /// <param name="privateKeyXml">the xml string for the private key</param>
        /// <param name="symmetricKey">the base64 encoded symmetric key</param>
        /// <returns>the base64 encoded string of the encoded private key</returns>
        public string Encrypt(string privateKeyXml, string symmetricKey)
        {
            using (var aes = new AesCryptoServiceProvider()
            {
                Key = Convert.FromBase64String(symmetricKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {

                var input = Encoding.UTF8.GetBytes(privateKeyXml);
                aes.GenerateIV();
                var iv = aes.IV;
                using (var encrypter = aes.CreateEncryptor(aes.Key, iv))
                using (var cipherStream = new MemoryStream())
                {
                    using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                    using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                    {
                        //Prepend IV to data
                        //tBinaryWriter.Write(iv); This is the original broken code, it encrypts the iv
                        cipherStream.Write(iv);  //Write iv to the plain stream (not tested though)
                        tBinaryWriter.Write(input);
                        tCryptoStream.FlushFinalBlock();
                    }

                    return Convert.ToBase64String(cipherStream.ToArray());
                }
            }
        }
    }
}