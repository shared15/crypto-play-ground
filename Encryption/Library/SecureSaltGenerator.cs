using System;
using System.Security.Cryptography;

namespace Library
{
    public class SecureSaltGenerator
    {
        private const int DEFAULT = 8;
        
        public string GenerateSecureSalt(int size = DEFAULT)
        {
            byte[] salt = new byte[size];
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }
    }
}