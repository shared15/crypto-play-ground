using System.Security.Cryptography;
using System;
using System.Text;
using System.IO;

namespace Library
{
    public class PrivateKeyDecryptor
    {
        /// <summary>
        /// Returns the private key XML.
        /// </summary>
        /// <param name="privateKey64">the base64 encoded prvivate key XML.</param>
        /// <param name="symmetricKey64">the base 64 encoded symmetric key.</param>
        /// <returns>the decoded private key XML.</returns>
        public string Decrypt(string privateKey64, string symmetricKey64)
        {
            using (var aes = new AesCryptoServiceProvider()
            {
                Key = Convert.FromBase64String(symmetricKey64),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                //get first 16 bytes of IV and use it to decrypt
                var iv = new byte[16];
                var input = Convert.FromBase64String(privateKey64);
                Array.Copy(input, 0, iv, 0, iv.Length);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, iv), CryptoStreamMode.Write))
                    using (var binaryWriter = new BinaryWriter(cs))
                    {
                        //Decrypt Cipher Text from Message
                        binaryWriter.Write(
                            input,
                            iv.Length,
                            input.Length - iv.Length
                        );
                    }

                    return Encoding.Default.GetString(ms.ToArray());
                }
            }
        }
    }
}