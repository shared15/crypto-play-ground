using System;
using System.Security.Cryptography;
using System.Text;

namespace Library
{
    public class DataDecryption : IDisposable
    {
        private readonly RSACryptoServiceProvider _rsaCryptoProvider;
        private readonly UTF8Encoding _converter;

        public DataDecryption()
        {
            _rsaCryptoProvider = new RSACryptoServiceProvider();
            _converter = new UTF8Encoding();
        }

        public string Decrypt(string data, string privateKeyXml)
        {
            _rsaCryptoProvider.FromXmlString(privateKeyXml);
            var bytes = Convert.FromBase64String(data);
            var decrypted = _rsaCryptoProvider.Decrypt(bytes, false);
            return _converter.GetString(decrypted);
        }

        public void Dispose()
        {
            _rsaCryptoProvider.Dispose();
        }
    }
}